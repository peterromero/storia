<section class="film_title">
  <div class="width_limiter">
    <div class="block_container">
      <h1 class="block col_9">14 StroNg</h1>
      <h2 class="block col_9">Produced by Val Orr &nbsp;|&nbsp; October
        2016</h2>
      <div class="block col_3 social">
        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo($self_url); ?>?v=<?php echo($timestamp); ?>" class="facebook" title="Share on Facebook" target="_blank">Facebook</a>
        <a href="https://twitter.com/share?text=<?php echo($tweet_text); ?>&amp;url=<?php echo($self_url); ?>" class="twitter share" title="Share on Twitter" target="_blank">Twitter</a>
        <a href="https://plus.google.com/share?url=<?php echo($self_url); ?>?v=<?php echo($timestamp); ?>" class="googleplus" title="Share on Google Plus" target="_blank">Google
          Plus</a>
      </div>
    </div>
  </div>
</section>
<section class="film">
  <div class="width_limiter">
    <div class="block_container">
      <div class="block full video aspect_16_9">
        <iframe src="https://player.vimeo.com/video/189059331?title=0&amp;byline=0&amp;portrait=0&amp;color=e32447&amp;api=1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        <div class="social_overlay">
          <div class="content">
            <h3>Share this Storia</h3>
            <a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo($self_url); ?>" class="facebook share" title="Share on Facebook" target="_blank">Facebook</a>
            <a href="https://twitter.com/share?text=<?php echo($tweet_text); ?>&amp;url=<?php echo($self_url); ?>" class="twitter share" title="Share on Twitter" target="_blank">Twitter</a>
            <a href="https://plus.google.com/share?url=<?php echo($self_url); ?>" class="googleplus share" title="Share on Google Plus" target="_blank">Google
              Plus</a>
            <a href="#" class="replay">watch again</a>
          </div>
        </div>
      </div>
      <a href="#" class="credits_link block full">Credits</a>
    </div>
  </div>
</section>
<section class="credits">
  <div class="width_limiter">
    <div class="block_container">
      <div class="block col_6">
        <div class="line">
          <div class="block col_3 of_6 leader">creator/producer</div>
          <div class="block col_3 of_6 name">VAL ORR</div>
        </div>
        <div class="line">
          <div class="block col_3 of_6 leader">directors</div>
          <div class="block col_3 of_6 name">VAL ORR</div>
        </div>
        <div class="line">
          <div class="block col_3 of_6 leader">camera</div>
          <div class="block col_3 of_6 name">PHILIP VAN DRUNEN</div>
        </div>
        <div class="line">
          <div class="block col_3 of_6 leader">voice over writer</div>
          <div class="block col_3 of_6 name">VAL ORR</div>
        </div>
        <div class="line">
          <div class="block col_3 of_6 leader">editor</div>
          <div class="block col_3 of_6 name">PHILIP VAN DRUNEN</div>
        </div>
        <div class="line">
          <div class="block col_3 of_6 leader">aerials</div>
          <div class="block col_3 of_6 name">CLOUDGATE AERIAL CINEMATOGRAPHY
          </div>
        </div>
        <div class="line">
          <div class="block col_3 of_6 leader">music</div>
          <div class="block col_3 of_6 name">"Kingdom" by Jordan Critz</div>
        </div>
        <div class="line">
          <div class="block col_3 of_6 leader">voice over</div>
          <div class="block col_3 of_6 name">VAL ORR</div>
        </div>
        <div class="line">
          <div class="block col_3 of_6 leader">voice consult</div>
          <div class="block col_3 of_6 name">PAT McNULTY</div>
        </div>
        <div class="line">
          <div class="block col_3 of_6 leader">graphic artist</div>
          <div class="block col_3 of_6 name">PETER ROMERO</div>
        </div>
        <div class="line">
          <div class="block col_3 of_6 leader">girl on horse</div>
          <div class="block col_3 of_6 name">BELLA ORR</div>
        </div>
        <div class="line">
          <div class="block col_3 of_6 leader">horse</div>
          <div class="block col_3 of_6 name">BUDDY</div>
        </div>
      </div>
      <div class="block col_6">
        <div class="line">
          <div class="line">
            <div class="block col_3 of_6 leader">cast</div>
            <div class="block col_3 of_6 name">
              CHERYL BREWSTER<br>
              TARISSA CAMPBELL<br>
              AMY COOKE<br>
              ANNE KERN<br>
              MARGE KLEIN<br>
              TAMMY KLEIN<br>
              ALTA ORR<br>
              VAL ORR<br>
              ESMERALDA RAMIREZ-RAY<br>
              JESSICA RAY<br>
              RONI BELL SYLVESTER<br>
              DRENDA VAN WYHE-THOEN<br>
              COLLEEN G. WHITLOW
            </div>
          </div>
        </div>
      </div>
      <div class="block full">
        <div class="pullout">aerials filmed on location at ORR RANCH, GRANBY COLORADO</div>
        <div class="pullout">for authorized use of this film, please contact <a href="mailto:storialife@gmail.com">storialife@gmail.com</a></div>
      </div>
    </div>
  </div>
</section>
<section class="message">
  <div class="width_limiter">
    <div class="block_container">
      <p class="block full balance-text">America, this land so beautiful it can
        bring tears to your eyes. Yet the land cannot match the beauty of the
        people living free.</p>
      <p class="block full balance-text">One of the essential joys of being an
        American is the freedom to speak —
        it brings us in contact with the spirit and beauty of our founding
        principles. To see the power of freedom, one only need listen to the
        people.</p>
      <p class="block full balance-text">We, strong independent-thinking women
        empower ourselves, enhanced by
        pride and love of our country. We possess a courage heightened by
        challenge. We are the guardians of freedom.</p>
      <p class="block full balance-text note">We are private American
        citizens. We are real people, not actors. We hold no elected offices. We
        were not asked to make this film, nor did we ask permission to make this
        film. We came together of our own free will. We spoke from our hearts
        and minds; these are our own individual thoughts with no script. We were
        not paid, nor have we received donations; an individual paid for this
        film.</p>
    </div>
  </div>
</section>
