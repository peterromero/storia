<section class="film_title">
	<div class="width_limiter">
		<div class="block_container">
			<h1 class="block col_9">chriStmAs RidE</h1>
			<h2 class="block col_9">Produced by Val Orr &nbsp;|&nbsp; December 2014</h2>
			<div class="block col_3 social">
				<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo($self_url); ?>?v=<?php echo($timestamp); ?>" class="facebook" title="Share on Facebook" target="_blank">Facebook</a>
				<a href="https://twitter.com/share?text=<?php echo($tweet_text); ?>&amp;url=<?php echo($self_url); ?>" class="twitter share" title="Share on Twitter" target="_blank">Twitter</a>
				<a href="https://plus.google.com/share?url=<?php echo($self_url); ?>?v=<?php echo($timestamp); ?>" class="googleplus" title="Share on Google Plus" target="_blank">Google Plus</a>
			</div>
		</div>
	</div>
</section>
<section class="film">
	<div class="width_limiter">
		<div class="block_container">
			<div class="block full video aspect_blayne">
				<iframe src="https://player.vimeo.com/video/115062433?title=0&amp;byline=0&amp;portrait=0&amp;color=e32447&amp;api=1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				<div class="social_overlay">
					<div class="content">
						<h3>Share this Storia</h3>
						<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo($self_url); ?>" class="facebook share" title="Share on Facebook" target="_blank">Facebook</a>
						<a href="https://twitter.com/share?text=<?php echo($tweet_text); ?>&amp;url=<?php echo($self_url); ?>" class="twitter share" title="Share on Twitter" target="_blank">Twitter</a>
						<a href="https://plus.google.com/share?url=<?php echo($self_url); ?>" class="googleplus share" title="Share on Google Plus" target="_blank">Google Plus</a>
						<a href="#" class="replay">watch again</a>
					</div>
				</div>
			</div>
			<a href="#" class="credits_link block full">Credits</a>
		</div>
	</div>
</section>
<section class="credits">
	<div class="width_limiter">
		<div class="block_container">
			<div class="block col_6">
				<div class="line">
					<div class="block col_3 of_6 leader">Creator/Producer</div>
					<div class="block col_3 of_6 name">Val Orr</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Director</div>
					<div class="block col_3 of_6 name">Blayne Chastain</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Director of Photography</div>
					<div class="block col_3 of_6 name">Philip Van Drunen</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Script &amp; Concept Advisor</div>
					<div class="block col_3 of_6 name">Rob Walker</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Editor</div>
					<div class="block col_3 of_6 name">Blayne Chastain</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Aerials</div>
					<div class="block col_3 of_6 name">Cloudgate Aerial Cinematography</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Wrangler</div>
					<div class="block col_3 of_6 name">Nikki Morrow</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Music</div>
					<div class="block col_3 of_6 name">"Riverside" by Blake Ewing</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Set Design</div>
					<div class="block col_3 of_6 name">Ed &amp; Susie Orr</div>
				</div>
			</div>
			<div class="block col_6">
				<div class="line">
					<div class="block col_3 of_6 leader">Production Assistant</div>
					<div class="block col_3 of_6 name">Susie Orr</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Writer</div>
					<div class="block col_3 of_6 name">Val Orr</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Costume Designer</div>
					<div class="block col_3 of_6 name">Val Orr</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Graphic Artist</div>
					<div class="block col_3 of_6 name">Peter Romero</div>
				</div>
				<div class="line">
					<div class="block col_6 of_6 section_title">Cast</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Val Orr</div>
					<div class="block col_3 of_6 name">Herself</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Bella Orr</div>
					<div class="block col_3 of_6 name">Herself</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Val's Horse</div>
					<div class="block col_3 of_6 name">Denny</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Bella's Horse</div>
					<div class="block col_3 of_6 name">Rogue</div>
				</div>
			</div>
			<div class="block full">
				<div class="pullout">filmed on location at ORR RANCH, GRANBY COLORADO</div>
			</div>
		</div>
	</div>
</section>
<section class="message">
	<div class="width_limiter">
		<div class="block_container">
			<p class="block full balance-text">Growing up at the foot of the Continental Divide on this Rocky Mountain cattle ranch was amazing… and hard.</p>
			<p class="block full balance-text">White winters with icy raw winds turned into hard-working, green summers with gentle breezes blowing the timothy hay, the warm sun shining brightly.</p>
			<p class="block full balance-text">This land, that my pioneer great-grandparents homesteaded in 1883, is home.</p>
			<p class="block full balance-text">I love this land.</p>
			<p class="block full balance-text">It represents freedom and the pioneer spirit that resides in all of us. This land is America.</p>
			<p class="block full balance-text">May the spirit of Christmas bring you peace and hope, the warmth of Christmas fill your heart with love, and the joy of Christmas kindle the fire of freedom for the next generation.</p>
			<p class="block full balance-text signature">Val Orr</p>
		</div>
	</div>
</section>
