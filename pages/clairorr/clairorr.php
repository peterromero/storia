<section class="film_title">
	<div class="width_limiter">
		<div class="block_container">
			<h1 class="block col_9">clAiR's walK</h1>
			<h2 class="block col_9">Produced by Val Orr &nbsp;|&nbsp; June 2015</h2>
			<div class="block col_3 social">
				<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo($self_url); ?>?v=<?php echo($timestamp); ?>" class="facebook" title="Share on Facebook" target="_blank">Facebook</a>
				<a href="https://twitter.com/share?text=<?php echo($tweet_text); ?>&amp;url=<?php echo($self_url); ?>" class="twitter share" title="Share on Twitter" target="_blank">Twitter</a>
				<a href="https://plus.google.com/share?url=<?php echo($self_url); ?>?v=<?php echo($timestamp); ?>" class="googleplus" title="Share on Google Plus" target="_blank">Google Plus</a>
			</div>
		</div>
	</div>
</section>
<section class="film">
	<div class="width_limiter">
		<div class="block_container">
			<div class="block full video aspect_blayne">
				<iframe src="https://player.vimeo.com/video/131845055?title=0&amp;byline=0&amp;portrait=0&amp;color=e32447&amp;api=1" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
				<div class="social_overlay">
					<div class="content">
						<h3>Share this Storia</h3>
						<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo($self_url); ?>" class="facebook share" title="Share on Facebook" target="_blank">Facebook</a>
						<a href="https://twitter.com/share?text=<?php echo($tweet_text); ?>&amp;url=<?php echo($self_url); ?>" class="twitter share" title="Share on Twitter" target="_blank">Twitter</a>
						<a href="https://plus.google.com/share?url=<?php echo($self_url); ?>" class="googleplus share" title="Share on Google Plus" target="_blank">Google Plus</a>
						<a href="#" class="replay">watch again</a>
					</div>
				</div>
			</div>
			<a href="#" class="credits_link block full">Credits</a>
		</div>
	</div>
</section>
<section class="credits">
	<div class="width_limiter">
		<div class="block_container">
			<div class="block col_6">
				<div class="line">
					<div class="block col_3 of_6 leader">Creator/Producer</div>
					<div class="block col_3 of_6 name">Val Orr</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Director</div>
					<div class="block col_3 of_6 name">Val Orr </div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Director of Photography</div>
					<div class="block col_3 of_6 name">Blayne Chastain</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Editors</div>
					<div class="block col_3 of_6 name">Blayne Chastain<br/>Val Orr</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Music</div>
					<div class="block col_3 of_6 name">“Processional” by Josh Garrels </div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Writers</div>
					<div class="block col_3 of_6 name">Val Orr<br/>Glenn Miller</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Spiritual Advisor</div>
					<div class="block col_3 of_6 name">Ray Shoop</div>
				</div>
			</div>
			<div class="block col_6">
				<div class="line">
					<div class="block col_3 of_6 leader">Advisors</div>
					<div class="block col_3 of_6 name">Tonya Perez<br/>Kaycee Hoffman<br/>Katie Egbert</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Graphic Artist</div>
					<div class="block col_3 of_6 name">Peter Romero</div>
				</div>
				<div class="line">
					<div class="block col_6 of_6 section_title">Cast</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Teen Clair</div>
					<div class="block col_3 of_6 name">Kody Shoop</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Voice of Clair</div>
					<div class="block col_3 of_6 name">Ray Shoop</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Man’s hands</div>
					<div class="block col_3 of_6 name">Coy Egbert</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Child’s hands</div>
					<div class="block col_3 of_6 name">Kingston Egbert</div>
				</div>
				<div class="line">
					<div class="block col_3 of_6 leader">Horse</div>
					<div class="block col_3 of_6 name">Maverick</div>
				</div>
			</div>
			<div class="block full">
				<div class="line">
					<div class="block col_6 of_12 leader">Wardrobe</div>
					<div class="block col_6 of_12 name">Clair Orr's Shirt &amp; Chaps</div>
				</div>
				<div class="line">
					<div class="block col_6 of_12 leader">Props</div>
					<div class="block col_6 of_12 name">Clair Orr's Saddle &amp; Bible</div>
				</div>
				<div class="line">
					<div class="block col_6 of_12 leader">Scripture Verse</div>
					<div class="block col_6 of_12 name">Ephesians 2:8-9</div>
				</div>
			</div>
			<div class="block full">
				<div class="pullout">filmed on location at CLAIR &amp; DEB ORR RANCH, KERSEY COLORADO and ED &amp; SUSIE ORR RANCH, GREELEY COLORADO</div>
			</div>
		</div>
	</div>
</section>
<section class="message">
	<div class="width_limiter">
		<div class="block_container">
			<p class="block full balance-text">Clair Orr grew up at the foot of the Continental Divide on a Rocky Mountain cattle ranch that his great-grandparents homesteaded in 1883.</p>
			<p class="block full balance-text">It was on this ranch at the age of 18 that Clair found God… in a pasture, kneeling on one knee, hand outstretched to Heaven.</p>
			<p class="block full balance-text">From that moment until his last day on earth, Clair walked by Faith, was grounded in Faith, and saved by Grace.</p>
			<p class="block full balance-text">His legacy: passing this Faith and belief in Jesus Christ to the next generation.</p>
		</div>
	</div>
</section>
