<!DOCTYPE html>
<html xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/" prefix="og: http://ogp.me/ns#">
<head>
<?php echo($google_fonts); ?>
<title><?php echo(config::$site_name . ' :: ' . $page_title); ?></title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="minimum-scale=1.0, maximum-scale=1.0, width=device-width, user-scalable=no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon" href="favicon.ico?v=<?php echo($timestamp); ?>" type="image/x-icon">

<!-- Facebook -->
<meta property="og:title" content="<?php echo($social_title); ?>">
<meta property="og:site_name" content="<?php echo(config::$site_name); ?>">
<meta property="og:url" content="<?php echo($self_url); ?>">
<meta property="og:description" content="<?php echo($short_description); ?>">
<meta property="og:image" content="<?php echo($image_directory . "/facebook.jpg?v=$timestamp"); ?>">
<meta property="og:type" content="video.other">
<meta property="og:locale" content="en_US">

<!-- Twitter -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:title" content="<?php echo($social_title); ?>">
<meta name="twitter:description" content="<?php echo($short_description); ?>">
<meta name="twitter:image:src" content="<?php echo($image_directory . "/twitter.jpg?v=$timestamp"); ?>">

<link rel="stylesheet" type="text/css" href="css/normalize.css">
<!-- Set create_col_classes to 1 and specify a number of columns with col=n if you want to generate grid classes for all the combinations of column widths available (for farming for another site, for example.) -->
<link rel="stylesheet" type="text/css" href="css/grid.css.php?create_col_classes=1&amp;cols=12">
<link rel="stylesheet" type="text/css" href="css/master.css">
<link rel="stylesheet" type="text/css" href="css/master_responsive.css">
<?php echo($css_files); ?>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	ga('create', 'UA-57021960-6', 'auto');
	ga('send', 'pageview');
</script>
<script type="text/javascript" src="js/main.js"></script>
<?php echo($js_files); ?>

</head>
<body>
<?php echo($page); ?>
</body>
</html>
