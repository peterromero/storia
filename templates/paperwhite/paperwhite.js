$(function() {
	$('header.main .learn_more').click(function(e) {
		e.preventDefault();
		$(this).toggleClass('open');
		if ($(this).hasClass('open')) {
			$('section.cta_top').animate({
				height: 'show',
				paddingTop: 'show',
				paddingBottom: 'show',
				marginTop: 'show',
				marginBottom: 'show',
				opacity: 1
			}, 300, function() {
				$('section.cta_top #name').focus();
			});
		} else {
			$('section.cta_top').animate({
				height: 'hide',
				paddingTop: 'hide',
				paddingBottom: 'hide',
				marginTop: 'hide',
				marginBottom: 'hide',
				opacity: 0
			}, 300);
		}
	});
	$('.credits_link').click(function(e) {
		e.preventDefault();
		$(this).toggleClass('open');
		if ($(this).hasClass('open')) {
			$('section.credits').animate({
				height: 'show',
				paddingTop: 'show',
				paddingBottom: 'show',
				marginTop: 'show',
				marginBottom: 'show',
				opacity: 1
			}, 300);
		} else {
			$('section.credits').animate({
				height: 'hide',
				paddingTop: 'hide',
				paddingBottom: 'hide',
				marginTop: 'hide',
				marginBottom: 'hide',
				opacity: 0
			}, 300);
		}
	});
	$('.submit').click(function() {
		if (!$(this).parents('form').find('.name').val() || !$(this).parents('form').find('.email').val()) {
			$('#dialog').text('Please enter both a name and an email address.').dialog({
				title: 'Missing Information',
				modal: true,
				buttons: {
					'OK': function() {$(this).dialog('close');}
				}
			});
		} else {
			$.ajax({
				url: '/ajax/contact.php',
				type: 'POST',
				data: $(this).parents('form').serialize(),
				dataType: 'json'
			}).done(function (data) {
				$('#dialog').text(data.message).dialog({
					title: data.title,
					modal: true,
					buttons: {
						'OK': function() {$(this).dialog('close');}
					}
				});
			});
		}
	});
});
$(window).load(function() {
	vertically_center_social_overlay_content();
	var $vimeo_player = $f($('.video iframe')[0]);
	$vimeo_player.addEvent('finish', function() {
		$('.video .social_overlay').fadeIn(300);
	});
	$('.replay').click(function(e) {
		e.preventDefault();
		$('.video .social_overlay').fadeOut(300, function() {
			$vimeo_player.api('play');
		});
	});
});
function vertically_center_social_overlay_content() {
	var container = $('.social_overlay');
	var content = $('.social_overlay .content');
	container.show();
	var margin = (container.height() - content.height())/2;
	container.hide();
	content.css('margin-top', margin);
}
