<header class="main">
	<div class="width_limiter">
		<div class="block_container">
			<a href="/" title="Storia" class="logo block col_1">Storia</a>
			<a href="#" class="learn_more block col_3">tell your Storia</a>
		</div>
	</div>
</header>
<section class="cta_top">
	<div class="width_limiter">
		<div class="block_container">
			<div class="block col_6 writeup">
				<p class="balance-text">More than just a film production company, Storia is about communicating from the heart. Fill out the form to find out how you can tell your own Storia.</p>
				<p class="tagline">Storia | <em>Moving</em> Pictures<sup>TM</sup></p>
			</div>
			<form>
				<div class="block col_6">
					<div>
						<div class="block col_6 of_6 bottom_space">
							<input type="text" value="" name="name" placeholder="Your Name" class="block col_3 of_6 name">
							<input type="text" value="" name="email" placeholder="Email Address" class="block col_3 of_6 email">
						</div>
					</div>
					<div>
						<textarea name="message" placeholder="Message" class="block col_6 of_6 message bottom_space"></textarea>
					</div>
					<div>
						<div class="block col_6 of_6 ">
							<div class="block col_3 of_6 placeholder"></div>
							<input type="button" value="submit inquiry" class="block col_3 of_6 submit">
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</section>
<div class="break_margin_collapse">&nbsp;</div>
<?php echo($content); ?>
<footer class="main">
	<div class="width_limiter">
		<div class="block_container">
			<div class="block full">
				<div class="logo">Storia</div>
			</div>
			<div class="block col_6 writeup">
				<p class="balance-text">More than just a film production company, Storia is about communicating from the heart. Fill out the form to find out how you can tell your own Storia.</p>
				<p class="tagline">Storia | <em>Moving</em> Pictures<sup>TM</sup></p>
			</div>
			<form>
				<div class="block col_6">
					<div>
						<div class="block col_6 of_6 bottom_space">
							<input type="text" value="" name="name" placeholder="Your Name" class="block col_3 of_6 name">
							<input type="text" value="" name="email" placeholder="Email Address" class="block col_3 of_6 email">
						</div>
					</div>
					<div>
						<textarea name="message" placeholder="Message" class="block col_6 of_6 message bottom_space"></textarea>
					</div>
					<div>
						<div class="block col_6 of_6 ">
							<div class="block col_3 of_6 placeholder"></div>
							<input type="button" value="submit inquiry" class="block col_3 of_6 submit">
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</footer>
<div id="dialog"></div>