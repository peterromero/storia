<?php
spl_autoload_register('autoload');
function autoload($class) {
	global $relative_path_to_doc_root;
	if ($class == 'config') {
		include_once($relative_path_to_doc_root . 'config.php');
	} else {
		include_once($relative_path_to_doc_root . 'classes/' . $class . '.php');
	}
}
if (config::$show_php_errors) {
	error_reporting(E_ALL);
} else {
	error_reporting(0);
}
function pjr_print($what) {
	echo('<pre>' . print_r($what, true) . '</pre>');
}
function pjr_dump($what) {
	echo('<pre>');
	var_dump($what);
	echo('</pre>');
}
