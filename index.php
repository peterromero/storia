<?php
$relative_path_to_doc_root = '';
require_once($relative_path_to_doc_root . 'init_app.php');
$path = new pjr_path($_GET['path']);
$template = '';
switch ($path->get_string()) {
	case '/': {
		header('Location: /valorr');
		exit();
		break;
	}
	case '/404': {
		$template = 'blank';
		break;
	}
	case '/valorr': {
		$template = 'paperwhite';
		break;
	}
	case '/clairorr': {
		$template = 'paperwhite';
		break;
	}
  case '/14strong': {
    $template = 'paperwhite';
    break;
  }
}
$page = new pjr_page($path, $template);
if (!$page->content_file_exists()) {
	header('Location: /404');
	exit();
}
$page->render();
