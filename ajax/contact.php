<?php
$result = array();
require('../plugins/phpmailer/PHPMailerAutoload.php');
$mail = new PHPMailer();
$mail->SetFrom('info@storia.life', 'Storia Films');
$mail->AddAddress("vo1883@gmail.com");
$mail->Subject = "Inquiry from Storia Website";
$mail->Body = "Great news! You have received an inquiry from the Storia website.\n\nName: {$_POST['name']}\nEmail: {$_POST['email']}\n\nMessage: {$_POST['message']}";
$mail->WordWrap = 80;
if(!$mail->Send()) {
	$result['success'] = false;
	$result['title'] = 'Error Sending Inquiry';
	$result['message'] = 'Your message could not be sent. Please try again later. Error: ' . $mail->ErrorInfo;
} else {
	$result['success'] = true;
	$result['title'] = 'Inquiry Sent';
	$result['message'] = 'Thank you for contacting us. Your information has been sent to Storia and we will be in touch with you shortly.';
}

echo(json_encode($result));
