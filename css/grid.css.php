<?php
	$relative_path_to_doc_root = '../';
	require_once($relative_path_to_doc_root . 'init_app.php');
	header('Content-type: text/css');
?>
.width_limiter {
	max-width: <?php echo(config::$document_width); ?>px;
    margin: 0 auto;
	padding: 0;
}
.block_container {
	clear: both;
	width: 100%;
	margin: 0;
	padding: 0 <?php echo(config::$gutter_width/2/config::$document_width*100); ?>%;
	overflow: hidden;
}
.block {
	float: left;
}
.block_container .block_container {
	padding: 0;
}
.block.of_6 {
	margin-left: 2.2222222222222%;
	margin-right: 2.2222222222222%;
}
.block .block:first-child {
	margin-left: 0;
}
.block .block:last-child {
	margin-right: 0;
}
.block.placeholder {
	height: 1px;
}
.col_3.of_6 {
	width: 47.7777777777778%;
}
.col_6.of_6 {
	width: 100%;
}
<?php
	pjr_grid_maker::define_block(".full", 1, 1, pjr_grid_maker::NO_BUTT);
	if (array_key_exists('create_col_classes', $_GET)) {
		if ($_GET['create_col_classes']==1) {
			for ($i = 1; $i<=$_GET['cols']; $i++) {
				pjr_grid_maker::define_block(".col_{$i}", $i, $_GET['cols'], pjr_grid_maker::NO_BUTT);
			}
			for ($i = 1; $i<=$_GET['cols']; $i++) {
				pjr_grid_maker::define_block(".col_{$i}_butt", $i, $_GET['cols'], pjr_grid_maker::BUTT_BOTH);
			}
			for ($i = 1; $i<=$_GET['cols']; $i++) {
				pjr_grid_maker::define_block(".col_{$i}_butt_left", $i, $_GET['cols'], pjr_grid_maker::BUTT_LEFT);
			}
			for ($i = 1; $i<=$_GET['cols']; $i++) {
				pjr_grid_maker::define_block(".col_{$i}_butt_right", $i, $_GET['cols'], pjr_grid_maker::BUTT_RIGHT);
			}
		}
	}
?>
