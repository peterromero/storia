<?php
// PHP Grid System copyright ©2013 by Peter Romero.
// General document structure should be width_limiter > block_container > block [> block]. A block_container can be used within a block to group and clear inner blocks, but is not required.
class pjr_grid_maker {
	const NO_BUTT = 0;
	const BUTT_LEFT = 1;
	const BUTT_RIGHT = 2;
	const BUTT_BOTH = 3;
	public static function define_block() {
		// Use define_block and define_block_inner outside of class declarations in CSS, as in some cases they create multiple class & pseudo-class declarations.
		// Args are $selector, $num_columns, $total_columns, $butting
		// $butting is optional and defaults to NO_BUTT

		// Example: grid_maker::define_block('.col_three',3,16,NO_BUTT);
		// Example of non-width-specified block: grid_maker::define_block('.col_auto_width',0,0,NO_BUTT);

		if ($num_args = func_num_args() < 3) {
			trigger_error("define_block() function must have at least three arguments; $num_args passed", E_USER_ERROR);
		}
		$selector = func_get_arg(0);
		$num_columns = func_get_arg(1);
		$total_columns = func_get_arg(2);
		$butting = (func_num_args()>=4)?func_get_arg(3):self::NO_BUTT;
		if ($num_columns == 0) { //Set $num_columns = 0 to get a block with an unspecified width.
			$block_container_padding_px = config::$gutter_width / 2;
			$block_container_width_as_self_px = config::$document_width;
			$block_container_width_as_parent_px = $block_container_width_as_self_px - 2 * $block_container_padding_px;
			$margin_width_px = config::$gutter_width / 2;
			$margin_width_pct = $margin_width_px / $block_container_width_as_parent_px * 100;
			switch($butting) {
				case self::NO_BUTT: {
					echo("$selector {float: left; margin: 0 {$margin_width_pct}%;}\r\n");
					break;
				}
				case self::BUTT_LEFT: {
					echo("$selector {float: left; margin: 0 {$margin_width_pct}% 0 0;}\r\n");
					break;
				}
				case self::BUTT_RIGHT: {
					echo("$selector {float: left; width: {$column_width_pct}%; margin: 0 0 0 {$margin_width_pct}%;}\r\n");
					break;
				}
				case self::BUTT_BOTH: {
					echo("$selector {float: left; margin: 0 0 0 0;}\r\n");
					break;
				}
			}
		} else {
			switch ($butting) {
				case self::NO_BUTT: {
						$block_container_padding_px = config::$gutter_width / 2;
						$block_container_width_as_self_px = config::$document_width;
						$block_container_width_as_parent_px = $block_container_width_as_self_px - 2 * $block_container_padding_px;
						$column_width_px = ($block_container_width_as_parent_px/$total_columns - config::$gutter_width) * $num_columns + config::$gutter_width * ($num_columns - 1);
						$column_width_pct = $column_width_px / $block_container_width_as_parent_px * 100;
						$margin_width_px = config::$gutter_width / 2;
						$margin_width_pct = $margin_width_px / $block_container_width_as_parent_px * 100;
						echo("$selector {float: left; width: {$column_width_pct}%; margin-left: {$margin_width_pct}%; margin-right: {$margin_width_pct}%; }\r\n");
					break;
				}
				case self::BUTT_LEFT: {
						$block_container_padding_px = config::$gutter_width / 2;
						$block_container_width_as_self_px = config::$document_width;
						$block_container_width_as_parent_px = $block_container_width_as_self_px - 2 * $block_container_padding_px;
						$column_width_px = (($block_container_width_as_parent_px/$total_columns) - (config::$gutter_width/2)) * $num_columns + (config::$gutter_width/2) * ($num_columns-1);
						$column_width_pct = $column_width_px / $block_container_width_as_parent_px * 100;
						$margin_width_px = config::$gutter_width / 2;
						$margin_width_pct = $margin_width_px / $block_container_width_as_parent_px * 100;
						echo("$selector {float: left; width: {$column_width_pct}%; margin-left: 0; margin-right: {$margin_width_pct}%; }\r\n");
					break;
				}
				case self::BUTT_RIGHT: {
						$block_container_padding_px = config::$gutter_width / 2;
						$block_container_width_as_self_px = config::$document_width;
						$block_container_width_as_parent_px = $block_container_width_as_self_px - 2 * $block_container_padding_px;
						$column_width_px = (($block_container_width_as_parent_px/$total_columns) - (config::$gutter_width/2)) * $num_columns + (config::$gutter_width/2) * ($num_columns-1);
						$column_width_pct = $column_width_px / $block_container_width_as_parent_px * 100;
						$margin_width_px = config::$gutter_width / 2;
						$margin_width_pct = $margin_width_px / $block_container_width_as_parent_px * 100;
						echo("$selector {float: left; width: {$column_width_pct}%; margin-left: {$margin_width_pct}%; margin-right: 0;}\r\n");
					break;
				}
				case self::BUTT_BOTH: {
						$block_container_padding_px = config::$gutter_width / 2;
						$block_container_width_as_self_px = config::$document_width;
						$block_container_width_as_parent_px = $block_container_width_as_self_px - 2 * $block_container_padding_px;
						$column_width_px = ($block_container_width_as_parent_px/$total_columns) * $num_columns;
						$column_width_pct = $column_width_px / $block_container_width_as_parent_px * 100;
						echo("$selector {float: left; width: {$column_width_pct}%; margin-left: 0; margin-right: 0;}\r\n");
					break;
				}
			}
		}
	}
	public static function define_block_inner() {
		// Args are $selector, $num_columns_outer, $total_columns_outer, and $num_columns_inner

		// Example: define_block_inner('.my-div',8,12,2)

		if ($num_args = func_num_args() < 4) {
			trigger_error("define_block() function must have four arguments; $num_args passed", E_USER_ERROR);
		}
		$selector = func_get_arg(0);
		$num_columns_outer = func_get_arg(1);
		$total_columns_outer = func_get_arg(2);
		$num_columns_inner = func_get_arg(3);

		$outer_block_container_padding_px = config::$gutter_width / 2;
		$outer_block_container_width_as_self_px = config::$document_width;
		$outer_block_container_width_as_parent_px = $outer_block_container_width_as_self_px - 2 * $outer_block_container_padding_px;
		$outer_column_width_px = ($outer_block_container_width_as_parent_px/$total_columns_outer - config::$gutter_width) * $num_columns_outer + config::$gutter_width * ($num_columns_outer - 1);

		$inner_block_container_width_as_self_px = $outer_column_width_px;
		$inner_block_container_padding_px = 0;
		$inner_block_container_width_as_parent_px = $inner_block_container_width_as_self_px - 2 * $inner_block_container_padding_px;
		$inner_column_width_px = ($outer_column_width_px - config::$gutter_width * ($num_columns_outer-1)) / $num_columns_outer * $num_columns_inner + config::$gutter_width * ($num_columns_inner-1);
		$inner_column_width_pct = $inner_column_width_px / $inner_block_container_width_as_parent_px * 100;
		$inner_margin_width_px = config::$gutter_width / 2;
		$inner_margin_width_pct = $inner_margin_width_px / $inner_block_container_width_as_parent_px * 100;

		$blocks_per_row = floor($num_columns_outer / $num_columns_inner);

		echo("$selector {float: left; width: {$inner_column_width_pct}%; margin: 0 {$inner_margin_width_pct}%;}
{$selector}:nth-child({$blocks_per_row}n + 1) {margin-left: 0;}
{$selector}:nth-child({$blocks_per_row}n) {margin-right: 0;}");
	}
}