<?php
class pjr_database_connection {
	private function __construct() {
	}
	public static function get_instance($mode) {
		switch($mode) {
			case 'pdo': {
				try {
					$instance = new PDO("mysql:host=" . config::$database_server . ";dbname=" . config::$database_name . "", config::$database_username, config::$database_password);
					$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$instance->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
				} catch(PDOException $e) {
					trigger_error($e->getMessage(), E_USER_ERROR);
				}
				break;
			}
			case 'mysqli': {
				$instance = new mysqli(config::$database_server, config::$database_username, config::$database_password, config::$database_name);
				if ($instance->connect_errno) {
					$errors[] = 'Could not connect to database.';
				}
				break;
			}
			default: {
				trigger_error('Invalid mode specified for database_connection::get_instance', E_USER_ERROR);
			}
		}
		return $instance;
	}
}
