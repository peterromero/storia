<?php
class pjr_path {
	private $path_array;
	function __construct($path_str) {
		$this->load_from_string($path_str);
	}
	function load_from_string($str) {
		$this->path_array = array_values(array_filter(explode('/', $str)));
	}
	function get_arg($n) {
		if (array_key_exists($n, $this->path_array)) {
			return $this->path_array[$n];
		} else {
			trigger_error("Nonexistent path argument ($n) specified", E_USER_WARNING);
			return '';
		}
	}
	function get_array() {
		return $this->path_array;
	}
	function get_string() {
		return '/' . implode('/', $this->path_array);
	}
	function get_slug() {
		return implode('_', $this->path_array);
	}
}
