<?php
class pjr_page {
	private $url;
	private $css_file_paths;
	private $js_file_paths;
	private $content_file_exists;
	private $content_file_path;
	private $content_info_exists;
	private $content_info_path;
	private $template_file_exists;
	private $template_file_path;
	function __construct($path, $template_name) {
		$this->url = $path;
		$this->css_file_paths = array();
		$this->js_file_paths = array();
		$slug = $path->get_slug();
		$this->content_file_exists = $this->set_content_file(config::$page_directory . "/$slug/$slug.php");
		$this->content_info_exists = $this->set_content_info(config::$page_directory . "/$slug/$slug.info");
		$this->template_file_exists = $this->set_template_file(config::$template_directory . "/$template_name/$template_name.php");
		$this->template_info_exists = $this->set_template_info(config::$template_directory . "/$template_name/$template_name.info");
		$this->add_css_file(config::$template_directory . "/$template_name/$template_name.css");
		$this->add_css_file(config::$template_directory . "/$template_name/{$template_name}_responsive.css");
		$this->add_css_file(config::$page_directory . "/$slug/$slug.css");
		$this->add_js_file(config::$template_directory . "/$template_name/$template_name.js");
		$this->add_js_file(config::$page_directory . "/$slug/$slug.js");
	}
	function add_css_file($path) {
		if (file_exists(config::$document_root . $path)) {
			$this->css_file_paths[] = $path;
		}
	}
	function add_js_file($path) {
		if (file_exists(config::$document_root . $path)) {
			$this->js_file_paths[] = $path;
		}
	}
	function set_content_file($path) {
		if (file_exists(config::$document_root . $path)) {
			$this->content_file_path = $path;
			return true;
		} else {
			return false;
		}
	}
	function set_content_info($path) {
		if (file_exists(config::$document_root . $path)) {
			$this->content_info_path = $path;
			return true;
		} else {
			return false;
		}
	}
	function set_template_file($path) {
		if (file_exists(config::$document_root . $path)) {
			$this->template_file_path = $path;
			return true;
		} else {
			return false;
		}
	}
	function set_template_info($path) {
		if (file_exists(config::$document_root . $path)) {
			$this->template_info_path = $path;
			return true;
		} else {
			return false;
		}
	}
	function render() {
		if (!$this->content_file_exists) {
			trigger_error("Cannot render page with missing content file", E_USER_ERROR);
		} else if (!$this->template_file_exists) {
			trigger_error("Cannot render page with missing template file", E_USER_ERROR);
		} else {
			//Default values that can be overridden in the page or template .info files
			$timestamp = time();
			$title = config::$site_name;
			$google_fonts = '';
			$self_url = config::$root_url . $this->url->get_string();
			$image_directory = config::$root_url . '/pages' . $this->url->get_string() . '/images';
			if ($this->content_info_exists) include(config::$document_root . $this->content_info_path);
			ob_start(); include(config::$document_root . $this->content_file_path); $content = ob_get_contents(); ob_end_clean();
			ob_start(); include(config::$document_root . $this->template_file_path); $page = ob_get_contents(); ob_end_clean();
			$css_files = $this->get_css_declarations();
			$js_files = $this->get_js_declarations();
			if ($this->template_info_exists) include(config::$document_root . $this->template_info_path);
			include(config::$document_root . config::$template_directory . '/html.php');
		}
	}
	function get_css_declarations() {
		$output = array();
		foreach($this->css_file_paths as $path) {
			$output[] = '<link rel="stylesheet" type="text/css" href="' . $path . '">';
		}
		return implode("\n", $output) . "\n";
	}
	function get_js_declarations() {
		$output = array();
		foreach($this->js_file_paths as $path) {
			$output[] = '<script type="text/javascript" src="' . $path . '"></script>';
		}
		return implode("\n", $output) . "\n";
	}
	function content_file_exists() {
		return $this->content_file_exists;
	}
}
